---
name: Nabeel Seedat
talks:
- "Introduction to Generative Adversarial Networks (GANs): hands-on to making new data (and some pretty pictures)"
---
* Deeply in love with deep learning and open-source
* Data Scientist at Shutterstock
* Completed a Masters in Machine Learning at Cornell University : working on
  Bayesian Deep Learning
* Completed a Masters at the University of the Witwatersrand (South Africa):
  working on Signal Processing and Machine Learning for Parkinson's Disease
  diagnosis
