---
name: Mark Smith
talks:
- "Make You An Async For Great Good!"
---
My name is Mark Smith, although I'm often known as Judy online. I'm a
Developer Advocate for Nexmo. I love writing stupid Python code in an
attempt to really understand how Python works. When I'm not doing this,
you'll find me crocheting, building custom keyboards, or designing models
for 3D printing.
