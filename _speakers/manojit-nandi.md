---
name: Manojit Nandi
talks:
- "The Benefits and Dangers of Face Recognition Technology"
---
I am a senior data scientist at J.P. Morgan Chase where I work on problems
related to algorithmic fairness in the finance industry. I love thinking
about how to use data science and machine learning to tackle problems with
high societal impact.
