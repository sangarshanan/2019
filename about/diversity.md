---
title: Diversity
---

## Everyone is welcome at {{ site.data.event.name }}
{{ site.data.event.name }} is committed to creating a welcoming and inclusive
environment for people of all backgrounds. We actively seek to increase the
diversity of our attendees and speakers. If you're a woman, a member of the
LGBTIA+ community, a person of color, or part of another underrepresented group
in the technical community, we encourage you to submit talks to and attend the
conference.

Are you a community organizer for a minority group in tech? We'd like to offer
your community members discounted tickets to PyGotham. Please contact
[organizers@pygotham.org](mailto:organizers@pygotham.org) for details.
